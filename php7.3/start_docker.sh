docker-compose up -d
docker exec -d php73_web_1 bash -c "cd /etc/apache2/sites-available && a2ensite '*.conf' && service apache2 reload"
docker exec -d php73_web_1 bash -c "mkdir /scripts && cd /scripts && touch hosts_script.sh && chmod +x hosts_script.sh"
docker exec -d php73_web_1 bash -c "echo 'for line $(ls /etc/) DO; echo $line >> text.txt; end' >> hosts_script.sh"
docker exec -d php73_web_1 bash -c "sh hosts_script.sh"
